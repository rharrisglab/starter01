﻿RESTORE FILELISTONLY  
FROM DISK = 'C:\vsprojects\starter01\starter01\App_Data\AdventureWorks2014.bak'  
GO  

--  AdventureWorks2014_Data
--  AdventureWorks2014_Log

RESTORE FILELISTONLY  
FROM DISK = 'C:\vsprojects\starter01\starter01\App_Data\Northwind.bak'  
GO  

--  Northwind
--  Northwind_log

RESTORE DATABASE AdventureWorks2014  
FROM DISK = 'C:\vsprojects\starter01\starter01\App_Data\AdventureWorks2014.bak'  
WITH MOVE 'AdventureWorks2014_Data' TO 'C:\vsprojects\starter01\starter01\App_Data\AdventureWorks2014.mdf',  
MOVE 'AdventureWorks2014_Log'       TO 'C:\vsprojects\starter01\starter01\App_Data\AdventureWorks2014.ldf'  



RESTORE DATABASE Northwind  
FROM DISK = 'C:\vsprojects\starter01\starter01\App_Data\Northwind.bak'  
WITH MOVE 'Northwind'               TO 'C:\vsprojects\starter01\starter01\App_Data\northwind.mdf',  
MOVE 'Northwind_log'                TO 'C:\vsprojects\starter01\starter01\App_Data\northwind.ldf'  

USE [AdventureWorks2014]
DBCC SHRINKDATABASE  ('AdventureWorks2014')
USE [Northwind]
DBCC SHRINKDATABASE  ('Northwind')
